##Autor: Axel ROUS
##Date of creation : 30/03/2023
##Input : packages phyloseq(16s ADN analysis), readxl (import xlsx file), tidyverse (data manipulation) and ape(read tree), Biom file, excel file of relative abundance and xlsx/csv metedatafile
##Output : phyloseq object from biom and/or excel file


library(phyloseq)
library(readxl)
library(tidyverse)
library(ape)
library(microbiome)
wd<-getwd()
{
biom_phylo<-import_biom(paste(wd,"/final.pick.Bacteria.an.0.03.biom",sep=""))  #import of biom file with results in absolute sequence reads
sample_names(biom_phylo)<-gsub("AR_","",gsub("GG_","",gsub("V3_","",sample_names(biom_phylo)))) #sample names formating for integration of metadatafile
  
  }## from biom
{
}##from excel with first row in dataframe containing columns names and second with total quantity of sequence read


excel_import <- read_excel(paste(wd,"/resume-exp03-cathomix-run31-32-33b.xlsx",sep=""))[-1,] #import of excel file
#Delete [-1,] if no row with total sequence read, result in relative

OTU_tab <- excel_import[,c(1,14:length(excel_import))] |> #(with bootstrap in impair column, if no BS in file, c(1,8:length(excel_import))
  column_to_rownames("OTU") 
names(OTU_tab)<-gsub("V3_","",gsub("GG_","",gsub("AR_","",names(OTU_tab)))) #Delete prefixe to format unification with metadatfile, can be modified, depend on your name format (V3 for primers, GG for Gaelle and AR for me)
OTU <- otu_table(OTU_tab,taxa_are_rows = T) #OTU table for phyloseq object


TAX_tab <- excel_import[,c(1,2,4,6,8,10,12)] |> 
  column_to_rownames("OTU")  #Taxonomy rank extraction (with bootstrap in impair column are deleted, if no BS in file, c(1:7))
TAX <- tax_table(as.matrix(TAX_tab)) #taxtable object for phyloseq object

excel_phylo<-phyloseq(OTU,TAX)  #phyloseq object from excel file


#import of metadatafile for integration to phyloseq object

meta <- read_xlsx(paste(wd,"Samples.xlsx",sep="/"),range="A1:L95") |> 
  column_to_rownames("Sample")

meta_map = sample_data(meta[1:78,][!str_detect(names(meta),"ARA")])#format metadata for integration in phyloseq object
rownames(meta_map)<-gsub("V3_","",rownames(meta_map)) #Names format with primers V3 prefixes extract to avoid risk of unrecognized sample

excel_phylo<-merge_phyloseq(excel_phylo,meta_map) #merge metadatafile in phyloseq object from excel
biom_phylo<-merge_phyloseq(biom_phylo,meta_map) #merge metadatafile in phyloseq object from excel

#import phylogenic tree from fasta file, using Clustal Omega 
tree=read.tree(file="phylogenetic_tree.txt")  #Read tree from fasta in ClustalW method and txt format
taxa_names(tree)<-str_extract(taxa_names(tree),"Otu[:digit:]{4}")  #Format name for better representation


excel_phylo<- merge_phyloseq(excel_phylo,tree)   
biom_phylo<- merge_phyloseq(biom_phylo,tree)   

biom_phylo_trim=prune_taxa(taxa_sums(biom_phylo)>15000, biom_phylo) ## Limitation of OTU quantity represented by absolute taxa sum higher than 15000 reads
rank_names(biom_phylo_trim)## print taxonomic rank to display in plot_tree function
plot_tree(biom_phylo_trim,label.tips = "Rank5",plot.margin=0.9,text.size=5) #plot by rank 5 (Family), type 

excel_phylo_trim=prune_taxa(genefilter_sample(excel_phylo, filterfun_sample(function(x) x > 0.1), A = 1),excel_phylo) ## Limitation of OTU quantity represented by relative abundance per OTU higher than 10% 
rank_names(excel_phylo_trim)## print taxonomic rank to display in plot_tree function
plot_tree(excel_phylo_trim,label.tips = "Family",plot.margin=0.9,text.size=5)
