Repository of Axel ROUS for analysis of 16S genes analysis 


Script and R project with exemple of use of phyloseq and ACP for analysis of 16S sequences.
Data in biom, xlsx, fasta and txt file

Script for importation of data from biom and xlsx files : import_data.R
Script for example of application of phyloseq with some filter and graphic representation : ACP auto.R

To add : Script to fix Order and Family (Same color panel for each family of a same order)
